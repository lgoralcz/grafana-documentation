# Getting Help

## Report an issue

To report an issue or ask for assistance, contact us at [Service Desk - PaaS-Web-App](https://cern.service-now.com/service-portal?id=service_element&name=PaaS-Web-App)

## General questions

For more general questions related to the application, we are also reachable under:

* [~Openshift](https://mattermost.web.cern.ch/it-dep/channels/openshift) channel in Mattermost.
* Central IT Discourse forum at the [Openshift category](https://discourse.web.cern.ch/c/web-hosting-development/openshift).
