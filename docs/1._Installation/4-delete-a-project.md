# Delete a project

In order to delete a project, go to the [Web Services Portal](https://webservices-portal.web.cern.ch/my-sites) and search for the project you want to remove.

You may need to use the [Web Services Portal Dev](https://webservices-portal-dev.web.cern.ch/my-sites) if you created the site using this dev portal.

Once you find the project, click on it to access the management page, and click on `Delete` button.

![image-29.png](../assets/image-29.png)

A confirmation screen will appear before the site is actually deleted, once you confirm it, your project will eventually be deleted in the coming seconds.

!!! info
    Be sure that indeed, you want to delete the project. This is a **destructive operation** and possibilities of recovering things are practically nonexistent.
