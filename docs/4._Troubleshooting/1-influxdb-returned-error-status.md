# InfluxDB returned error status: 401 Unauthorized

This is a known issue, and it has beed reported at [grafana/grafana#37626](https://github.com/grafana/grafana/issues/37626), and it is due to the change of the model of managing the passwords internally by Grafana, according to [grafana/grafana#37626 (comment)](https://github.com/grafana/grafana/issues/37626#issuecomment-895226853).

How the error log looks like:

```bash
...
t=2021-11-11T08:00:20+0000 lvl=eror msg="Alert Rule Result Error" logger=alerting.evalContext ruleId=2 name="xxx: xxx" error="request handler error: failed to query data: InfluxDB returned error status: 401 Unauthorized" changing state to=alerting
...
```

Solution:

Go to your instance and login as `administrator`, then navigate to Configuration (engineering wheel) > Data Sources and re-type all passwords of your each of the datasources (specially those using the InfluxDB datasource).
