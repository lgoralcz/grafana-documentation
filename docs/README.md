# Introduction

This document describes how to create an instance of Grafana.

Refer to the diferent topics for a more detailed explanation about how to install and to configure a Grafana instance.

## Service Level

A self-service [Grafana operator](https://cern.service-now.com/service-portal?id=kb_article&n=KB0004363) is provided by IT to help CERN users with quickly provisioning their own dedicated Grafana instances (e.g., to visualize data from an InfluxDB database) in central web servers. IT provides the infrastructure and a Grafana operator with support for SSO, but users are in charge of administering and maintaining their instance.

!!! info
    **No expertise is provided for Grafana configuration, and support is limited to the infrastructure** (excluding for instance troubleshooting of issues with using Grafana).

For new releases, IT deployes the latest image from upstream with a semi-automated process. Versions of Grafana are centrally managed, so users will expect upgrades from time to time, with no action from their side. Note that, although we offer such possibility, **it is unsupported to have Grafana instances stuck on a specific version**.

The operator provisions by default CERN SSO authentication and provides instructions about how to configure access control with grappa groups. See this documentation for instructions about how to deploy a Grafana instance in the OKD4 infrastructure.

The operator is also maintained by the CERN community with best-effort support.

### Communication

- `Announcements`: Formerly, users were invited to subscribe to the `openshift-grafana-users` e-group. This e-group was used for communication, including announcements for new versions of the operator. For the time being, an [SSB](https://ssb.web.cern.ch) will be written everytime there is an upgrade of the Grafana instances. This will change with the new IT communication strategy.
- The [Discourse forum about Openshift](https://discourse.web.cern.ch/c/web-hosting-development/openshift/8) is the preferred place for questions about Grafana configuration and issues.
- The [~Openshift](https://mattermost.web.cern.ch/it-dep/channels/openshift) Mattermost channel is also a place for questions concerning Grafana.
- To contribute to the Grafana operator, please visit its [GitLab project page](https://gitlab.cern.ch/db/grafana-operator) in GitLab.

This is provided in complement to the [Grafana instance used for Central Monitoring](https://cern.service-now.com/service-portal?id=kb_article&n=KB0005191) and a [Puppet shared module](https://gitlab.cern.ch/ai/it-puppet-module-grafana) that enables deploying Grafana on dedicated Virtual Machines.
