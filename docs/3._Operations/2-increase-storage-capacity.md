# Increase storage capacity

It might happens that your Grafana instance is running out of capacity for its storage. Any user can automatically and **reponsably** increase the storage capacity for Grafana instance by editing the Custom Resource at their best convenience as follows:

While in the `Administrator` environment, and after selecting the target `Project`, click on `Installed Operators`, and then click on the `Grafana Operator` link:

![image-30.png](../assets/image-30.png)

In the new window, click on the `Grafana` tab.

![image-31.png](../assets/image-31.png)

In the new window, click on the name of your Grafana instance.

![image-32.png](../assets/image-32.png)

In the new window, click on the `YAML` tab.

![image-33.png](../assets/image-33.png)

Go to the Custom Resource and edit the `dataStorage.size` element by setting a different and **greater** value, like `10Gi`, as per the following image.

![image-34.png](../assets/image-34.png)

Finally, `Ctrl + S` or click on the <kbd>Save</kbd> button and the value will be automatically and instantaneously populated to the corresponding persistent volume claim.

That's it, your Grafana instance is now enjoying of more storage capacity.

!!! warning
    Pay attention to not set a value higher than the quota (by default 100 GiB). To know more about quotas refer to [user docs](https://paas.docs.cern.ch/6._Quota_and_resources/1-project-quota/).
