# Install extra plugins

Extra Grafana plugins can be installed as follows:

1. Go to <https://app-catalogue.cern.ch>
2. Select your Grafana project
3. Make sure you are in the `Administrator` view (not `Developer`) then navigate to `Workloads` -> `Pods`
4. Select the Grafana pod (there should be only one)
5. Select `Terminal` tab
6. Install the plugin using the plugin's installation instructions. It will typically be a command such as: `grafana-cli plugins install my-plugin`. Simply run the relevant commands in the terminal window.
7. Complete installation by running `kill 1` to restart the container and thus Grafana itself
