# Grafana user-facing documentation

This pages contains useful information about how to install and configure a Grafana instance. They are intended for user's usage.

This documentation is deployed under <https://grafana.docs.cern.ch>.
